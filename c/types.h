#pragma once

typedef struct { float x, y, z; } float3_t;
typedef struct { float x, y, z, w; } float4_t;

#include <stddef.h>

typedef struct { ptrdiff_t start, count; } workload_t;
