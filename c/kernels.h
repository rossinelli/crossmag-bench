#include "types.h"

void crossmag_soa (
	const float * const x0,
	const float * const y0,
	const float * const z0,
	const float * const x1,
	const float * const y1,
	const float * const z1,
	const ptrdiff_t n,
	float * const out );

void crossmag_float3 (
	const float3_t * const in0,
	const float3_t * const in1,
	const ptrdiff_t n,
	float * const out );

void crossmag_float4 (
	const float4_t * const in0,
	const float4_t * const in1,
	const ptrdiff_t n,
	float * const out );
