#include <stddef.h>
#include <math.h>

#include "kernels.h"

void crossmag_soa (
	const float * const x0,
	const float * const y0,
	const float * const z0,
	const float * const x1,
	const float * const y1,
	const float * const z1,
	const ptrdiff_t n,
	float * const out )
{
	for (ptrdiff_t i = 0; i < n; ++i)
	{
		const float xu = x0[i];
		const float yu = y0[i];
		const float zu = z0[i];

		const float xv = x1[i];
		const float yv = y1[i];
		const float zv = z1[i];

		const float u2 = xu * xu + yu * yu + zu * zu;
		const float v2 = xv * xv + yv * yv + zv * zv;
		const float uv = xu * xv + yu * yv + zu * zv;

		const float a2 = fmaxf(0.f, u2 * v2 - uv * uv);

		out[i] = sqrtf(a2);
	}
}

void crossmag_float3 (
	const float3_t * const in0,
	const float3_t * const in1,
	const ptrdiff_t n,
	float * const out )
{
	for (ptrdiff_t i = 0; i < n; ++i)
	{
		const float xu = in0[i].x;
		const float yu = in0[i].y;
		const float zu = in0[i].z;

		const float xv = in1[i].x;
		const float yv = in1[i].y;
		const float zv = in1[i].z;

		const float u2 = xu * xu + yu * yu + zu * zu;
		const float v2 = xv * xv + yv * yv + zv * zv;
		const float uv = xu * xv + yu * yv + zu * zv;

		const float a2 = fmaxf(0.f, u2 * v2 - uv * uv);

		out[i] = sqrtf(a2);
	}
}

void crossmag_float4 (
	const float4_t * const in0,
	const float4_t * const in1,
	const ptrdiff_t n,
	float * const out )
{
	for (ptrdiff_t i = 0; i < n; ++i)
	{
		const float xu = in0[i].x;
		const float yu = in0[i].y;
		const float zu = in0[i].z;

		const float xv = in1[i].x;
		const float yv = in1[i].y;
		const float zv = in1[i].z;

		const float u2 = xu * xu + yu * yu + zu * zu;
		const float v2 = xv * xv + yv * yv + zv * zv;
		const float uv = xu * xv + yu * yv + zu * zv;

		const float a2 = fmaxf(0.f, u2 * v2 - uv * uv);

		out[i] = sqrtf(a2);
	}
}
