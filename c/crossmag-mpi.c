#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

#include <math.h>
#include <string.h>

#include <mpi.h>

#include "kernels.h"
#include "macros.h"
#include "util.h"

#ifndef MPI_CHECK
#define MPI_CHECK(stmt)								\
	do												\
	{												\
		const int code = stmt;						\
													\
		if (code != MPI_SUCCESS)					\
		{											\
			char msg[2048];							\
			int len = sizeof(msg);					\
			MPI_Error_string(code, msg, &len);		\
													\
			fprintf(stderr,							\
					"ERROR\n" #stmt "%s (%s:%d)\n",	\
					msg, __FILE__, __LINE__);		\
													\
			fflush(stderr);							\
													\
			MPI_Abort(MPI_COMM_WORLD, code);		\
		}											\
	}												\
	while(0)
#endif

size_t count_entries (
	MPI_Comm comm,
    const char * const pn,
    const size_t esz )
{
    int r;
    MPI_CHECK(MPI_Comm_rank(comm, &r));

    size_t retval = -1;

    if (!r)
    {
        MPI_File f;
        MPI_CHECK(MPI_File_open(MPI_COMM_SELF, (char *)pn,
                                MPI_MODE_RDONLY, MPI_INFO_NULL, &f));

		MPI_Offset fsz;
		MPI_CHECK(MPI_File_get_size(f, &fsz));

		CHECK(fsz % esz == 0,
			  "error: file size (%zu) not multiple of element size (%zu)\n",
			  (size_t)fsz, esz);

        retval = fsz / esz;

        MPI_CHECK(MPI_File_close(&f));
    }

    MPI_CHECK(MPI_Bcast(&retval, 1, MPI_INT64_T, 0, MPI_COMM_WORLD));

	return retval;
}

void * collective_read (
	MPI_Comm comm,
	const char * const pn,
    const size_t offset,
	const size_t footprint)
{
	MPI_File f;
	MPI_CHECK(MPI_File_open(comm, (char *)pn, MPI_MODE_RDONLY, MPI_INFO_NULL, &f));

	void * retval = NULL;
	POSIX_CHECK(retval = malloc(footprint));

	MPI_CHECK(MPI_File_read_at_all
			  (f, offset, retval, footprint, MPI_BYTE, MPI_STATUS_IGNORE));

	MPI_CHECK(MPI_File_close(&f));

	return retval;
}

int main (
	const int argc,
	const char * argv[])
{
	MPI_CHECK(MPI_Init((int *)&argc, (char ***)&argv));

	MPI_Comm comm = MPI_COMM_WORLD;

	int r, rc;
	MPI_CHECK(MPI_Comm_rank(comm, &r));
	MPI_CHECK(MPI_Comm_size(comm, &rc));

	if (4 != argc)
	{
		if (!r)
			fprintf(stderr,
					"usage: %s <path/to/a_xyz.raw> <path/to/b_xyz.raw> <path/to/result.raw>\n",
					argv[0]);

		MPI_CHECK(MPI_Finalize());

		return EXIT_FAILURE;
	}

	const ptrdiff_t ec0 = count_entries(comm, argv[1], sizeof(float3_t));
	const ptrdiff_t ec1 = count_entries(comm, argv[2], sizeof(float3_t));

	CHECK(ec0 == ec1,
		  "%s: mismatch of file size between <%s> and <%s>\n",
		  argv[0], argv[1], argv[2]);

	ptrdiff_t ec = ec0;

	workload_t load = divide_workload(r, rc, ec);

	const ptrdiff_t lec = load.count;

	float3_t * in0 = collective_read(comm, argv[1], sizeof(*in0) * load.start, sizeof(*in0) * lec);
	float3_t * in1 = collective_read(comm, argv[2], sizeof(*in1) * load.start, sizeof(*in1) * lec);

	float * out = NULL;
	POSIX_CHECK(out = malloc(sizeof(*out) * lec));

	int REPS = 1;
	READENV(REPS, atoi);

	const char * KER = "";
	READENV(KER, );

	double tts = HUGE_VAL;

	if (!strcmp("soa", KER))
	{
		if (!r)
			fprintf(stderr, "%s: soa kernel\n", argv[0]);

		float * tmp[2][3] = { {NULL, NULL, NULL}, {NULL, NULL, NULL} };

		for (int c = 0; c < 6; ++c)
			POSIX_CHECK(tmp[c / 3][c % 3] = malloc(sizeof(float) * lec));

		to_soa(in0, lec, tmp[0][0], tmp[0][1], tmp[0][2]);
		to_soa(in1, lec, tmp[1][0], tmp[1][1], tmp[1][2]);

		MPI_CHECK(MPI_Barrier(comm));

		const double t0 = rdtss();

		for (int t = 0; t < REPS; ++t)
			crossmag_soa(tmp[0][0], tmp[0][1], tmp[0][2],
						 tmp[1][0], tmp[1][1], tmp[1][2],
						 lec, out );

		MPI_CHECK(MPI_Barrier(comm));

		tts = rdtss() - t0;

		for (int c = 5; c >= 0; --c)
			free(tmp[c / 3][c % 3]);
	}
	else if (!strcmp("float4", KER))
	{
		if (!r)
			fprintf(stderr, "%s: float4 kernel\n", argv[0]);

		float4_t *tmp0 = NULL, *tmp1 = NULL;
		POSIX_CHECK(tmp0 = malloc(sizeof(*tmp0) * lec));
		POSIX_CHECK(tmp1 = malloc(sizeof(*tmp1) * lec));

		to_float4(in0, lec, tmp0);
		to_float4(in1, lec, tmp1);

		MPI_CHECK(MPI_Barrier(comm));

		const double t0 = rdtss();

		for (int t = 0; t < REPS; ++t)
			crossmag_float4(tmp0, tmp1, lec, out);

		MPI_CHECK(MPI_Barrier(comm));

		tts = rdtss() - t0;

		free(tmp1);
		free(tmp0);
	}
	else
	{
		if (!r)
			fprintf(stderr, "%s: float3 kernel\n", argv[0]);

		MPI_CHECK(MPI_Barrier(comm));

		const double t0 = rdtss();

		for (int t = 0; t < REPS; ++t)
			crossmag_float3(in0, in1, lec, out);

		MPI_CHECK(MPI_Barrier(comm));

		tts = rdtss() - t0;
	}

	/* write results to file */
	{
		MPI_File fout;

		MPI_CHECK(MPI_File_open(comm, argv[3],
								MPI_MODE_WRONLY | MPI_MODE_CREATE,
								MPI_INFO_NULL, &fout));

		MPI_CHECK(MPI_File_set_size(fout, sizeof(float) * ec));

		MPI_CHECK(MPI_File_write_at_all(fout, sizeof(float) * load.start,
										out, sizeof(float) * lec, MPI_BYTE, MPI_STATUS_IGNORE));

		MPI_CHECK(MPI_File_close(&fout));
	}

	free(out);
	free(in1);
	free(in0);

	MPI_CHECK(MPI_Finalize());

	if (!r)
		fprintf(stderr,
				"%s: average TTS: %g s, BW: %g GB/s\n",
				argv[0],
				tts / REPS,
				REPS * ec * (sizeof(*in0) + sizeof(*in1) + sizeof(*out)) / 1e9 / tts);

	return EXIT_SUCCESS;
}
