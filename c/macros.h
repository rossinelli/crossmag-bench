#pragma once

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

#include <stdlib.h>

#define READENV(x, op)							\
	do											\
	{											\
		if (getenv(#x))							\
			x = op(getenv(#x));					\
	}											\
	while(0)

#define TOSTR_(a) #a
#define MKSTR(a) TOSTR_(a)

#include <stdio.h>

#define POSIX_CHECK(stmt)							\
	do												\
	{												\
		if (!(stmt))								\
		{											\
			perror(#stmt  " in "					\
				   __FILE__ ":" MKSTR(__LINE__) );	\
													\
			exit(EXIT_FAILURE);						\
		}											\
	}												\
	while(0)

#define CHECK(stmt, ...)						\
	do											\
	{											\
		if (!(stmt))							\
		{										\
			fprintf(stderr,						\
					__VA_ARGS__);				\
												\
			exit(EXIT_FAILURE);					\
		}										\
	}											\
	while(0)
