#include <stddef.h>

#include <sys/time.h>

#include "macros.h"
#include "util.h"

double rdtss()
{
	struct timeval tv;
	POSIX_CHECK(0 == gettimeofday(&tv, NULL));

	return tv.tv_sec + 1e-6 * tv.tv_usec;
}

size_t posix_read (
	const char * const pathname,
	const size_t esz,
	void ** ptr)
{
	FILE * f = NULL;
	POSIX_CHECK(f = fopen(pathname, "rb"));

	POSIX_CHECK(0 == fseek(f, 0, SEEK_END));
	const size_t fsz = ftell(f);

	POSIX_CHECK(0 == fseek(f, 0, SEEK_SET));

    CHECK(fsz % esz == 0,
		  "error: file size (%zu) not multiple of element size (%zu)\n",
		  fsz, esz);

	void * buf = NULL;
	POSIX_CHECK(buf = malloc(fsz));

	POSIX_CHECK(1 == fread(buf, fsz, 1, f));
	POSIX_CHECK(0 == fclose(f));

	*ptr = buf;

	return fsz / esz;
}

workload_t divide_workload (
    const ptrdiff_t w,
    const ptrdiff_t wn,
    const ptrdiff_t tn)
{
	const ptrdiff_t share = tn / wn;
	const ptrdiff_t rmnd = tn % wn;

    return (workload_t){ .start = share * w + MIN(w, rmnd), .count = share + (w < rmnd) };
}

void to_soa (
	const float3_t * in,
	const ptrdiff_t n,
	float * x,
	float * y,
	float * z)
{
	for (ptrdiff_t i = 0; i < n; ++i)
	{
		x[i] = in[i].x;
		y[i] = in[i].y;
		z[i] = in[i].z;
	}
}

void to_float4 (
	const float3_t * in,
	const ptrdiff_t n,
	float4_t * out )
{
	for (ptrdiff_t i = 0; i < n; ++i)
	{
		out[i].x = in[i].x;
		out[i].y = in[i].y;
		out[i].z = in[i].z;
		out[i].w = 0;
	}
}
