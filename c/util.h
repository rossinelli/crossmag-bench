#include <stddef.h>
#include "types.h"

double rdtss();

size_t posix_read (
	const char * const pathname,
	const size_t elementsize,
	void ** ptr );

workload_t divide_workload (
    const ptrdiff_t worker,
    const ptrdiff_t workercount,
    const ptrdiff_t taskcount );

void to_soa (
	const float3_t * in,
	const ptrdiff_t count,
	float * x,
	float * y,
	float * z );

void to_float4 (
	const float3_t * in,
	const ptrdiff_t count,
	float4_t * out );
