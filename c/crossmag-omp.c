#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>

#include <math.h>
#include <string.h>
#include <sys/time.h>

#include <omp.h>

#include "macros.h"
#include "util.h"
#include "kernels.h"

int main (
	const int argc,
	const char * argv[])
{
#pragma omp parallel
	{
		if (!omp_get_thread_num())
			printf("%s: considering %d openmp threads\n",
				   argv[0], omp_get_num_threads());
	}

	if (4 != argc)
	{
		fprintf(stderr,
				"usage: %s <path/to/a_xyz.raw> <path/to/b_xyz.raw> <path/to/result.raw>\n",
				argv[0]);

		return EXIT_FAILURE;
	}

	float3_t *in0 = NULL, *in1 = NULL;

	const ptrdiff_t ec0 = posix_read(argv[1], sizeof(*in0), (void **)&in0);
	const ptrdiff_t ec1 = posix_read(argv[2], sizeof(*in1), (void **)&in1);

	CHECK(ec0 == ec1,
		  "%s: mismatch of file size between <%s> and <%s>\n",
		  argv[0], argv[1], argv[2]);

	const ptrdiff_t ec = ec0;

	float * out = NULL;
	POSIX_CHECK(out = malloc(sizeof(*out) * ec));

	int REPS = 1;
	READENV(REPS, atoi);

	const char * KER = "";
	READENV(KER, );

	double tts = HUGE_VAL;

	if (!strcmp("soa", KER))
	{
		fprintf(stderr, "%s: soa kernel\n", argv[0]);

		float * tmp[2][3] = { {NULL, NULL, NULL}, {NULL, NULL, NULL} };

		for (int c = 0; c < 6; ++c)
			POSIX_CHECK(tmp[c / 3][c % 3] = malloc(sizeof(float) * ec));

		/* NUMA-aware transposition */
#pragma omp parallel
		{
			const workload_t load = divide_workload(omp_get_thread_num(), omp_get_num_threads(), ec);

			to_soa(in0 + load.start, load.count, tmp[0][0] + load.start, tmp[0][1] + load.start, tmp[0][2] + load.start);
			to_soa(in1 + load.start, load.count, tmp[1][0] + load.start, tmp[1][1] + load.start, tmp[1][2] + load.start);
		}

		const double t0 = rdtss();

#pragma omp parallel
		{
			const workload_t load = divide_workload(omp_get_thread_num(), omp_get_num_threads(), ec);

			for (int t = 0; t < REPS; ++t)
				crossmag_soa(tmp[0][0] + load.start, tmp[0][1] + load.start, tmp[0][2] + load.start,
							 tmp[1][0] + load.start, tmp[1][1] + load.start, tmp[1][2] + load.start,
							 load.count, out + load.start);
		}

		tts = rdtss() - t0;

		for (int c = 5; c >= 0; --c)
			free(tmp[c / 3][c % 3]);
	}
	else if (!strcmp("float4", KER))
	{
		fprintf(stderr, "%s: float4 kernel\n", argv[0]);

		float4_t *tmp0 = NULL, *tmp1 = NULL;
		POSIX_CHECK(tmp0 = malloc(sizeof(*tmp0) * ec));
		POSIX_CHECK(tmp1 = malloc(sizeof(*tmp1) * ec));

		/* NUMA-aware float3-float4 casting */
#pragma omp parallel
		{
			const workload_t load = divide_workload(omp_get_thread_num(), omp_get_num_threads(), ec);

			to_float4(in0 + load.start, load.count, tmp0 + load.start);
			to_float4(in1 + load.start, load.count, tmp1 + load.start);
		}

		const double t0 = rdtss();

#pragma omp parallel
		{
			const workload_t load = divide_workload(omp_get_thread_num(), omp_get_num_threads(), ec);

			for (int t = 0; t < REPS; ++t)
				crossmag_float4(tmp0 + load.start, tmp1 + load.start,
								load.count, out + load.start);
		}

		tts = rdtss() - t0;

		free(tmp1);
		free(tmp0);
	}
	else
	{
		fprintf(stderr, "%s: float3 kernel\n", argv[0]);

		const double t0 = rdtss();

#pragma omp parallel
		{
			const workload_t load = divide_workload(omp_get_thread_num(), omp_get_num_threads(), ec);

			for (int t = 0; t < REPS; ++t)
				crossmag_float3(in0 + load.start, in1 + load.start,
								load.count, out + load.start);
		}

		tts = rdtss() - t0;
	}

	/* write output to file */
	{
		FILE * f = NULL;
		POSIX_CHECK(f = fopen(argv[3], "wb"));
		POSIX_CHECK(1 == fwrite(out, sizeof(*out) * ec, 1, f));
		POSIX_CHECK(0 == fclose(f));
	}

	free(out);
	free(in1);
	free(in0);

	fprintf(stderr,
			"%s: average TTS: %g s, BW: %g GB/s\n",
			argv[0],
			tts / REPS,
			REPS * ec * (sizeof(*in0) + sizeof(*in1) + sizeof(*out)) / 1e9 / tts);

	return EXIT_SUCCESS;
}
