#!/usr/bin/env bash
set -e

echo "compiling..."
make

if [ ! -f a-small.raw ]
then
	echo "generating input files.."
	../genfloats.py
fi

function check_accuracy
{
	psrc=$1

	echo "is output comparable to the reference output? "
	diff <(od -tfF < $psrc | head) <(od -tfF < ref.raw | head)
	if [ $? -eq 0 ]
	then
		echo "yes"
	else
		echo "NO!!!!!!!!!"
	fi
}

MPI_NTASKS_DEFAULT=1
if [ "Darwin" == `uname -s` ]
then
	MPI_NTASKS_DEFAULT=`sysctl -n hw.ncpu`
else
	MPI_NTASKS_DEFAULT=`nproc`
fi

MPI_NTASKS=${MPI_NTASKS:-$MPI_NTASKS_DEFAULT}

function benchmark
{
	size=$1

	echo "========== BENCHMARK $size ==============="

	ln -sf a-$size.raw a.raw
	ln -sf b-$size.raw b.raw

	echo "generating reference output..."
	REP=1 KER=float3 ./crossmag a.raw b.raw ref.raw 2> /dev/null

	echo "running the benchmark..."
	for k in float3 float4 soa ;
	do
		echo "===== KERNEL $k"

		if [ -f ./crossmag-omp ]
		then
			echo "OpenMP :"
			KER=$k ./crossmag-omp a.raw b.raw $k.raw
			check_accuracy $k.raw
		fi

		echo "MPI :"
		KER=$k mpirun -n $MPI_NTASKS ./crossmag-mpi a.raw b.raw $k.raw
		check_accuracy $k.raw
	done
}

REP=10000 benchmark small
REP=1 benchmark large
