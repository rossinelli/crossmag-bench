#!/usr/bin/env python

import numpy as np
import numpy.random

(0.5 - np.random.rand(int(3 * 3e8))).astype(np.float32).tofile('a-large.raw')
(0.5 - np.random.rand(int(3 * 3e8))).astype(np.float32).tofile('b-large.raw')

(0.5 - np.random.rand(int(3 * 1e6))).astype(np.float32).tofile('a-small.raw')
(0.5 - np.random.rand(int(3 * 1e6))).astype(np.float32).tofile('b-small.raw')
