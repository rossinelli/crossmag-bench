#include <stdio.h>
#include <stddef.h>
#include <math.h>

#define CUDA_CHECK(stmt)												\
    do																	\
    {																	\
		cudaError_t retval = stmt;										\
																		\
		if (cudaSuccess != retval)										\
		{																\
			fprintf(stderr,												\
					"CUDA_CHECK FAILED: %s at %s:%d\n",					\
					cudaGetErrorString(retval), __FILE__, __LINE__);	\
																		\
			exit(EXIT_FAILURE);											\
		}																\
    }																	\
    while(0)

#include <macros.h>

static int ADDIO = 0;

static void __attribute__((constructor)) init()
{
	READENV(ADDIO, atoi);
}

#include "kernels-cuda.h"

__global__
void kcrossmag_soa (
	const float * __restrict__ const x0,
	const float * __restrict__ const y0,
	const float * __restrict__ const z0,
	const float * __restrict__ const x1,
	const float * __restrict__ const y1,
	const float * __restrict__ const z1,
	const ptrdiff_t n,
	float * const out )
{
	const ptrdiff_t i = threadIdx.x + blockDim.x * (ptrdiff_t)blockIdx.x;

	if (i >= n)
		return;

	const float xu = x0[i];
	const float yu = y0[i];
	const float zu = z0[i];

	const float xv = x1[i];
	const float yv = y1[i];
	const float zv = z1[i];

	const float u2 = xu * xu + yu * yu + zu * zu;
	const float v2 = xv * xv + yv * yv + zv * zv;
	const float uv = xu * xv + yu * yv + zu * zv;

	const float a2 = fmaxf(0.f, u2 * v2 - uv * uv);

	out[i] = sqrtf(a2);
}

extern "C"
double crossmag_soa_cuda (
	const int reps,
	const float * const x0,
	const float * const y0,
	const float * const z0,
	const float * const x1,
	const float * const y1,
	const float * const z1,
	const ptrdiff_t n,
	float * const outptr )
{
	cudaEvent_t ev0, ev1;
	CUDA_CHECK(cudaEventCreate(&ev0));
	CUDA_CHECK(cudaEventCreate(&ev1));

	float *out = NULL, *in[6] = { NULL, NULL, NULL, NULL, NULL, NULL };

	/* allocate input */
	{
		for (int c = 0; c < 6; ++c)
			CUDA_CHECK(cudaMalloc((void **)(in + c), sizeof(float) * n));

		CUDA_CHECK(cudaMalloc((void **) &out, sizeof(float) * n));
	}

	if (ADDIO)
		CUDA_CHECK(cudaEventRecord(ev0));

	/* upload input */
	for (int c = 0; c < 6; ++c)
	{
		const float * ptrs[] = { x0, y0, z0, x1, y1, z1 };

		CUDA_CHECK(cudaMemcpy(in[c], ptrs[c], sizeof(float) * n, cudaMemcpyHostToDevice));
	}

	const ptrdiff_t bs = 4 * 32;
	const ptrdiff_t gs = (n + bs - 1) / bs;

	if (!ADDIO)
		CUDA_CHECK(cudaEventRecord(ev0));

	for (int t = 0; t < reps; ++t)
		kcrossmag_soa <<< gs, bs >>>(in[0], in[1], in[2], in[3], in[4], in[5], n, out);

	if (!ADDIO)
		CUDA_CHECK(cudaEventRecord(ev1));

	CUDA_CHECK(cudaMemcpy(outptr, out, sizeof(float) * n, cudaMemcpyDeviceToHost));

	if (ADDIO)
		CUDA_CHECK(cudaEventRecord(ev1));

	float ttsms;
	CUDA_CHECK(cudaEventElapsedTime(&ttsms, ev0, ev1));

	CUDA_CHECK(cudaEventDestroy(ev1));
	CUDA_CHECK(cudaEventDestroy(ev0));

	for (int c = 5; c >= 0; --c)
		CUDA_CHECK(cudaFree(in[c]));

	return ttsms * 1e-3;
}

template <typename T>
__global__
void kcrossmag_aos (
	const T * const in0,
	const T * const in1,
	const ptrdiff_t n,
	float * const out )
{
	const ptrdiff_t i = threadIdx.x + blockDim.x * (ptrdiff_t)blockIdx.x;

	if (i >= n)
		return;

	const float xu = in0[i].x;
	const float yu = in0[i].y;
	const float zu = in0[i].z;

	const float xv = in1[i].x;
	const float yv = in1[i].y;
	const float zv = in1[i].z;

	const float u2 = xu * xu + yu * yu + zu * zu;
	const float v2 = xv * xv + yv * yv + zv * zv;
	const float uv = xu * xv + yu * yv + zu * zv;

	const float a2 = fmaxf(0.f, u2 * v2 - uv * uv);

	out[i] = sqrtf(a2);
}

template <typename T>
double dispatch (
	const int reps,
	const T * const in0,
	const T * const in1,
	const ptrdiff_t n,
	float * const outptr )
{
	cudaEvent_t ev0, ev1;
	CUDA_CHECK(cudaEventCreate(&ev0));
	CUDA_CHECK(cudaEventCreate(&ev1));

	T * in[2] = { NULL, NULL };
	float * out = NULL;

	/* allocate input */
	{
		for (int c = 0; c < 2; ++c)
			CUDA_CHECK(cudaMalloc((void **)(in + c), sizeof(T) * n));

		CUDA_CHECK(cudaMalloc((void **) &out, sizeof(float) * n));
	}

	if (ADDIO)
		CUDA_CHECK(cudaEventRecord(ev0));

	/* upload input */
	for (int c = 0; c < 2; ++c)
	{
		const T * ptrs[] = { in0, in1 };

		CUDA_CHECK(cudaMemcpy(in[c], ptrs[c], sizeof(T) * n, cudaMemcpyHostToDevice));
	}

	const ptrdiff_t bs = 4 * 32;
	const ptrdiff_t gs = (n + bs - 1) / bs;

	if (!ADDIO)
		CUDA_CHECK(cudaEventRecord(ev0));

	for (int t = 0; t < reps; ++t)
		kcrossmag_aos <<< gs, bs >>>(in[0], in[1], n, out);

	if (!ADDIO)
		CUDA_CHECK(cudaEventRecord(ev1));

	CUDA_CHECK(cudaMemcpy(outptr, out, sizeof(float) * n, cudaMemcpyDeviceToHost));

	if (ADDIO)
		CUDA_CHECK(cudaEventRecord(ev1));

	float ttsms;
	CUDA_CHECK(cudaEventElapsedTime(&ttsms, ev0, ev1));

	CUDA_CHECK(cudaEventDestroy(ev1));
	CUDA_CHECK(cudaEventDestroy(ev0));

	for (int c = 1; c >= 0; --c)
		CUDA_CHECK(cudaFree(in[c]));

	return ttsms * 1e-3;
}

extern "C"
double crossmag_float3_cuda (
	const int reps,
	const float3_t * const in0,
	const float3_t * const in1,
	const ptrdiff_t n,
	float * const out )
{
	return dispatch<float3>(reps, (float3 *)in0, (float3 *)in1, n, out);
}

extern "C"
double crossmag_float4_cuda (
	const int reps,
	const float4_t * const in0,
	const float4_t * const in1,
	const ptrdiff_t n,
	float * const out )
{
	return dispatch<float4>(reps, (float4 *)in0, (float4 *)in1, n, out);
}
