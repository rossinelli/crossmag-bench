#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

#include <math.h>
#include <string.h>

#include "kernels-cuda.h"
#include "macros.h"
#include "util.h"

int main (
	const int argc,
	const char * argv[])
{
	if (4 != argc)
	{
		fprintf(stderr,
				"usage: %s <path/to/a_xyz.raw> <path/to/b_xyz.raw> <path/to/result.raw>\n",
				argv[0]);

		return EXIT_FAILURE;
	}

	float3_t *in0 = NULL, *in1 = NULL;

	const ptrdiff_t ec0 = posix_read(argv[1], sizeof(*in0), (void **)&in0);
	const ptrdiff_t ec1 = posix_read(argv[2], sizeof(*in1), (void **)&in1);

	CHECK(ec0 == ec1,
		  "%s: mismatch of file size between <%s> and <%s>\n",
		  argv[0], argv[1], argv[2]);

	const ptrdiff_t ec = ec0;

	float * out = NULL;
	POSIX_CHECK(out = (float *)malloc(sizeof(*out) * ec));

	int REPS = 1;
	READENV(REPS, atoi);

	const char * KER = "";
	READENV(KER, );

	double tts = HUGE_VAL;

	if (!strcmp("soa", KER))
	{
		fprintf(stderr, "%s: soa kernel\n", argv[0]);

		float * tmp[2][3] = { {NULL, NULL, NULL}, {NULL, NULL, NULL} };

		for (int c = 0; c < 6; ++c)
			POSIX_CHECK(tmp[c / 3][c % 3] = (float *)malloc(sizeof(float) * ec));

		to_soa(in0, ec, tmp[0][0], tmp[0][1], tmp[0][2]);
		to_soa(in1, ec, tmp[1][0], tmp[1][1], tmp[1][2]);

		tts = crossmag_soa_cuda(REPS,
								tmp[0][0], tmp[0][1], tmp[0][2],
								tmp[1][0], tmp[1][1], tmp[1][2],
								ec, out );

		for (int c = 5; c >= 0; --c)
			free(tmp[c / 3][c % 3]);
	}
	else if (!strcmp("float4", KER))
	{
		fprintf(stderr, "%s: float4 kernel\n", argv[0]);

		float4_t *tmp0 = NULL, *tmp1 = NULL;
		POSIX_CHECK(tmp0 = (float4_t *)malloc(sizeof(*tmp0) * ec));
		POSIX_CHECK(tmp1 = (float4_t *)malloc(sizeof(*tmp1) * ec));

		to_float4(in0, ec, tmp0);
		to_float4(in1, ec, tmp1);

		tts = crossmag_float4_cuda(REPS, tmp0, tmp1, ec, out);

		free(tmp1);
		free(tmp0);
	}
	else
	{
		fprintf(stderr, "%s: float3 kernel\n", argv[0]);

		tts = crossmag_float3_cuda(REPS, in0, in1, ec, out);
	}

	/* write output to file */
	{
		FILE * f = NULL;
		POSIX_CHECK(f = fopen(argv[3], "wb"));
		POSIX_CHECK(1 == fwrite(out, sizeof(*out) * ec, 1, f));
		POSIX_CHECK(0 == fclose(f));
	}

	free(out);
	free(in1);
	free(in0);

	fprintf(stderr,
			"%s: average TTS: %g s, BW: %g GB/s\n",
			argv[0],
			tts / REPS,
			REPS * ec * (sizeof(*in0) + sizeof(*in1) + sizeof(*out)) / 1e9 / tts);

	return EXIT_SUCCESS;
}
