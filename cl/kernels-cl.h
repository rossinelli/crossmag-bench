#include "types.h"

double crossmag_soa_cl (
		const int reps,
		const float * const x0,
		const float * const y0,
		const float * const z0,
		const float * const x1,
		const float * const y1,
		const float * const z1,
		const ptrdiff_t n,
		float * const out );

double crossmag_float3_cl (
		const int reps,
		const float3_t * const in0,
		const float3_t * const in1,
		const ptrdiff_t n,
		float * const out );

double crossmag_float4_cl (
		const int reps,
		const float4_t * const in0,
		const float4_t * const in1,
		const ptrdiff_t n,
		float * const out );
