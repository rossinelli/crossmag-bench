#include <stdio.h>

#ifdef __APPLE__
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

#include "util-cl.h"

int main (
		const int argc,
		const char * const argv[] )
{
	/* get platforms */
	cl_platform_id ps[64];
	cl_uint pc = 0;
	CL_CHECK(clGetPlatformIDs(64, ps, &pc));

	for (cl_uint p = 0; p < pc; ++p)
	{
		cl_device_id ds[64];
		cl_uint dc = 0;

		CL_CHECK(clGetDeviceIDs(ps[p], CL_DEVICE_TYPE_ALL, 64, ds, &dc));

		/* print platform profile and version */
		{
			char version[1024], profile[1024];
			CL_CHECK(clGetPlatformInfo (ps[p], CL_PLATFORM_VERSION, sizeof(version), version, NULL));
			CL_CHECK(clGetPlatformInfo (ps[p], CL_PLATFORM_PROFILE, sizeof(profile), profile, NULL));

			printf("%s: platform %d\n%s: version %s\n%s: profile %s\n",
					argv[0], p, argv[0], version, argv[0], profile);
		}

		for (cl_uint d = 0; d < dc; ++d)
		{
			/* print device name */
			{
				char name[1024];
				CL_CHECK(clGetDeviceInfo(ds[d], CL_DEVICE_NAME, sizeof(name), name, NULL));
				printf("%s: platform %d device %d : %s\n", argv[0], (int)p, (int)d, name);
			}

			/* print few scalar integers info */
			{
				cl_device_info infos[] = {
					CL_DEVICE_MAX_COMPUTE_UNITS,
					CL_DEVICE_MAX_WORK_GROUP_SIZE,
					CL_DEVICE_MAX_MEM_ALLOC_SIZE
				};

				size_t values[3];
				for (int i = 0; i < 3; ++i)
					CL_CHECK(clGetDeviceInfo(ds[d], infos[i], sizeof(size_t), values + i, NULL));

				printf("%s: compute units: %d\n%s: max workgroup size: %zu\n%s: buffer capacity: %.2f GB\n",
						argv[0], (int)values[0], argv[0], values[1], argv[0], values[2] * 1e-9);
			}
		}
	}

	return EXIT_SUCCESS;
}
