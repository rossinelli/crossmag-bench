#include <stddef.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#ifdef __APPLE__
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

#include "macros.h"
#include "util-cl.h"
#include "kernels-cl.h"

#define READENV(x, op) \
	do \
{ \
	if (getenv(#x)) \
	x = op(getenv(#x)); \
} \
while(0)

cl_device_id dev;
cl_context ctx;
cl_command_queue cq;

static void init ()
{
	char * DEVICE = "0,0";
	READENV(DEVICE, );

	int iplatform, idevice;
	sscanf(DEVICE, "%d,%d", &iplatform, &idevice);

	/* pick device */
	{
		cl_platform_id platform;

		cl_uint __attribute__((unused)) pc, dc;

		/* pick platform */
		{
			cl_platform_id platforms[64];

			CL_CHECK(clGetPlatformIDs(64, platforms, &pc));
			platform = platforms[iplatform];
		}

		cl_device_id devices[64];
		CL_CHECK(clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 64, devices, &dc));

		dev = devices[idevice];
	}

	/* create context */
	{
		cl_int err;
		ctx = clCreateContext(NULL, 1, &dev, NULL, NULL, &err);
		CL_CHECK(err);
	}

	/* create command queue, set it as default */
	{
		cl_int err;
		cq =  clCreateCommandQueue(ctx, dev, CL_QUEUE_PROFILING_ENABLE, &err);
		CL_CHECK(err);
	}
}

static cl_kernel mkk (const char * src)
{
	const size_t lsrc = strlen(src);

	cl_int err;
	cl_program prog = clCreateProgramWithSource(ctx, 1, &src, &lsrc, &err);
	CL_CHECK(err);

	const char * OPTS = "-cl-finite-math-only -cl-mad-enable -cl-denorms-are-zero";
	READENV(OPTS, );

	fprintf(stderr,
			"compiling with options: <%s>\n",
			OPTS);

	err = clBuildProgram(prog, 1, &dev, OPTS, NULL, NULL);

	if (err == CL_BUILD_PROGRAM_FAILURE)
	{
		size_t logsz;
		CL_CHECK(clGetProgramBuildInfo(prog, dev, CL_PROGRAM_BUILD_LOG, 0, NULL, &logsz));

		char log[logsz];
		CL_CHECK(clGetProgramBuildInfo(prog, dev, CL_PROGRAM_BUILD_LOG, logsz, log, NULL));

		fprintf(stderr, "%s\n", log);
	}

	CL_CHECK(err);

	cl_kernel ker = clCreateKernel(prog, "k", &err);
	CL_CHECK(err);

	CL_CHECK(clRetainProgram(prog));

	return ker;
}

static double bench (
		const int reps,
		const ptrdiff_t n,
		cl_kernel ker )
{
	const size_t lsize = 128;
	const size_t gsize = lsize * ((n + lsize - 1) / lsize);

	cl_event ev0, ev1;
	CL_CHECK(clEnqueueNDRangeKernel(cq, ker, 1, NULL, &gsize, &lsize, 0, NULL, &ev0));

	for (int t = 0; t < reps - 2; ++t)
		CL_CHECK(clEnqueueNDRangeKernel(cq, ker, 1, NULL, &gsize, &lsize, 0, NULL, NULL));

	if (reps < 2)
		ev1 = ev0;
	else
		CL_CHECK(clEnqueueNDRangeKernel(cq, ker, 1, NULL, &gsize, &lsize, 0, NULL, &ev1));

	CL_CHECK(clWaitForEvents(1, &ev1));

	cl_ulong t0, t1;
	CL_CHECK(clGetEventProfilingInfo(ev0, CL_PROFILING_COMMAND_SUBMIT, sizeof(t0), &t0, NULL));
	CL_CHECK(clGetEventProfilingInfo(ev1, CL_PROFILING_COMMAND_END, sizeof(t1), &t1, NULL));

	const double tts = t1 * 1e-9 - t0 * 1e-9;

	if (ev1 != ev0)
		CL_CHECK(clReleaseEvent(ev1));

	CL_CHECK(clReleaseEvent(ev0));

	return tts;
}

static void cleanup ()
{
	CL_CHECK(clReleaseCommandQueue(cq));
	CL_CHECK(clReleaseContext(ctx));
}

double crossmag_soa_cl (
		const int reps,
		const float * const x0,
		const float * const y0,
		const float * const z0,
		const float * const x1,
		const float * const y1,
		const float * const z1,
		const ptrdiff_t n,
		float * const outptr )
{
	init();

	cl_int err;
	cl_mem in[6], out;

	/* create buffer objects */
	{
		const float * ptrs[] = {x0, y0, z0, x1, y1, z1 };

		for (int c = 0; c < 6; ++c)
		{
			in[c] = clCreateBuffer(ctx, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR | CL_MEM_HOST_NO_ACCESS, n * sizeof(float), (void *)ptrs[c], &err);
			CL_CHECK(err);
		}

		out = clCreateBuffer(ctx, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY, n * sizeof(*outptr), NULL, &err);
		CL_CHECK(err);
	}

	cl_kernel ker = mkk(
			"kernel void k (\n"
			"	const global float * x0,\n"
			"	const global float * y0,\n"
			"	const global float * z0,\n"
			"	const global float * x1,\n"
			"	const global float * y1,\n"
			"	const global float * z1,\n"
			"	const ulong n,\n"
			"	global float * out )\n"
			"{\n"
			"	const ulong i = get_global_id(0);\n"
			"\n"
			"	if (i >= n)\n"
			"		return;\n"
			"\n"
			"	const float xu = x0[i];\n"
			"	const float yu = y0[i];\n"
			"	const float zu = z0[i];\n"
			"\n"
			"	const float xv = x1[i];\n"
			"	const float yv = y1[i];\n"
			"	const float zv = z1[i];\n"
			"\n"
			"	const float u2 = xu * xu + yu * yu + zu * zu;\n"
			"	const float v2 = xv * xv + yv * yv + zv * zv;\n"
			"	const float uv = xu * xv + yu * yv + zu * zv;\n"
			"\n"
			"	const float a2 = fmax(0.f, u2 * v2 - uv * uv);\n"
			"\n"
			"	out[i] = sqrt(a2);\n"
			"}");

	for (int i = 0; i < 6; ++i)
		CL_CHECK(clSetKernelArg(ker, i, sizeof(in[i]), &in[i]));

	CL_CHECK(clSetKernelArg(ker, 6, sizeof(n), &n));
	CL_CHECK(clSetKernelArg(ker, 7, sizeof(out), &out));

	const double tts = bench(reps, n, ker);

	CL_CHECK(clEnqueueReadBuffer(cq, out, CL_TRUE, 0, sizeof(*outptr) * n, outptr, 0, NULL, NULL));

	CL_CHECK(clReleaseKernel(ker));

	CL_CHECK(clReleaseMemObject(out));

	for (int i = 5; i >= 0; --i)
		CL_CHECK(clReleaseMemObject(in[i]));

	cleanup();

	return tts;
}

double crossmag_float3_cl (
		const int reps,
		const float3_t * const in0ptr,
		const float3_t * const in1ptr,
		const ptrdiff_t n,
		float * const outptr )
{
	init();

	cl_int err;
	cl_mem in0, in1, out;

	/* create buffer objects */
	{
		in0 = clCreateBuffer(ctx, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR | CL_MEM_HOST_NO_ACCESS, n * sizeof(*in0ptr), (void *)in0ptr, &err);
		CL_CHECK(err);

		in1 = clCreateBuffer(ctx, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR | CL_MEM_HOST_NO_ACCESS, n * sizeof(*in1ptr), (void *)in1ptr, &err);
		CL_CHECK(err);

		out = clCreateBuffer(ctx, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY, n * sizeof(*outptr), NULL, &err);
		CL_CHECK(err);
	}

	cl_kernel ker = mkk(
			"kernel void k (\n"
			"	const global float * in0,\n"
			"	const global float * in1,\n"
			"	const ulong n,\n"
			"	global float * out )\n"
			"{\n"
			"	const ulong i = get_global_id(0);\n"
			"\n"
			"	if (i >= n)\n"
			"		return;\n"
			"\n"
			"	const ulong base = 3 * i;\n"
			"\n"
			"	const float xu = in0[base + 0];\n"
			"	const float yu = in0[base + 1];\n"
			"	const float zu = in0[base + 2];\n"
			"\n"
			"	const float xv = in1[base + 0];\n"
			"	const float yv = in1[base + 1];\n"
			"	const float zv = in1[base + 2];\n"
			"\n"
			"	const float u2 = xu * xu + yu * yu + zu * zu;\n"
			"	const float v2 = xv * xv + yv * yv + zv * zv;\n"
			"	const float uv = xu * xv + yu * yv + zu * zv;\n"
			"\n"
			"	const float a2 = fmax(0.f, u2 * v2 - uv * uv);\n"
			"\n"
			"	out[i] = sqrt(a2);\n"
			"}");

	CL_CHECK(clSetKernelArg(ker, 0, sizeof(in0), &in0));
	CL_CHECK(clSetKernelArg(ker, 1, sizeof(in1), &in1));
	CL_CHECK(clSetKernelArg(ker, 2, sizeof(n), &n));
	CL_CHECK(clSetKernelArg(ker, 3, sizeof(out), &out));

	const double tts = bench(reps, n, ker);

	CL_CHECK(clEnqueueReadBuffer(cq, out, CL_TRUE, 0, sizeof(*outptr) * n, outptr, 0, NULL, NULL));

	CL_CHECK(clReleaseKernel(ker));

	CL_CHECK(clReleaseMemObject(out));
	CL_CHECK(clReleaseMemObject(in1));
	CL_CHECK(clReleaseMemObject(in0));

	cleanup();

	return tts;
}

double crossmag_float4_cl (
		const int reps,
		const float4_t * const in0ptr,
		const float4_t * const in1ptr,
		const ptrdiff_t n,
		float * const outptr )
{
	init();

	cl_int err;
	cl_mem in0, in1, out;

	/* create buffer objects */
	{
		in0 = clCreateBuffer(ctx, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR | CL_MEM_HOST_NO_ACCESS, n * sizeof(*in0ptr), (void *)in0ptr, &err);
		CL_CHECK(err);

		in1 = clCreateBuffer(ctx, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR | CL_MEM_HOST_NO_ACCESS, n * sizeof(*in1ptr), (void *)in1ptr, &err);
		CL_CHECK(err);

		out = clCreateBuffer(ctx, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY, n * sizeof(*outptr), NULL, &err);
		CL_CHECK(err);
	}

	cl_kernel ker = mkk(
			"kernel void k (\n"
			"	const global float4 * in0,\n"
			"	const global float4 * in1,\n"
			"	const ulong n,\n"
			"	global float * out )\n"
			"{\n"
			"	const ulong i = get_global_id(0);\n"
			"\n"
			"	if (i >= n)\n"
			"		return;\n"
			"\n"
			"	const float xu = in0[i].x;\n"
			"	const float yu = in0[i].y;\n"
			"	const float zu = in0[i].z;\n"
			"\n"
			"	const float xv = in1[i].x;\n"
			"	const float yv = in1[i].y;\n"
			"	const float zv = in1[i].z;\n"
			"\n"
			"	const float u2 = xu * xu + yu * yu + zu * zu;\n"
			"	const float v2 = xv * xv + yv * yv + zv * zv;\n"
			"	const float uv = xu * xv + yu * yv + zu * zv;\n"
			"\n"
			"	const float a2 = fmax(0.f, u2 * v2 - uv * uv);\n"
			"\n"
			"	out[i] = sqrt(a2);\n"
			"}");

	CL_CHECK(clSetKernelArg(ker, 0, sizeof(in0), &in0));
	CL_CHECK(clSetKernelArg(ker, 1, sizeof(in1), &in1));
	CL_CHECK(clSetKernelArg(ker, 2, sizeof(n), &n));
	CL_CHECK(clSetKernelArg(ker, 3, sizeof(out), &out));

	const double tts = bench(reps, n, ker);

	CL_CHECK(clEnqueueReadBuffer(cq, out, CL_TRUE, 0, sizeof(*outptr) * n, outptr, 0, NULL, NULL));

	CL_CHECK(clReleaseKernel(ker));

	CL_CHECK(clReleaseMemObject(out));
	CL_CHECK(clReleaseMemObject(in1));
	CL_CHECK(clReleaseMemObject(in0));

	cleanup();

	return tts;
}
