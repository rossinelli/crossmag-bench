#!/usr/bin/env bash
set -e

echo "compiling..."
make

if [ ! -f a-small.raw ]
then
	echo "generating input files.."
	../genfloats.py
fi

function sloppy_od
{
	psrc=$1

	od -tfF < $psrc | head -100 | \
		awk '{ printf "$s %.4f %.4f %.4f %.4f\n", $1, $2, $3, $4, $5 }'
}

function check_accuracy
{
	psrc=$1

	echo "is output comparable to the reference output? "
	diff <(sloppy_od $psrc) <(sloppy_od ref.raw)

	if [ $? -eq 0 ]
	then
		echo "yes"
	else
		echo "NO!!!!!!!!!"
	fi
}

function benchmark
{
	size=$1

	echo "========== BENCHMARK $size ==============="

	ln -sf a-$size.raw a.raw
	ln -sf b-$size.raw b.raw

	echo "generating reference output..."
	REP=1 KER=float3 ../c/crossmag a.raw b.raw ref.raw 2> /dev/null

	echo "running the benchmark..."
	for k in float3 float4 soa ;
	do
		echo "===== KERNEL $k"

		echo "OpenCL "
		KER=$k ./crossmag-cl a.raw b.raw $k.raw
		check_accuracy $k.raw
	done
}

while read -r line
do
	args=($line)
	devicename=`echo $line | cut -d':' -f3,3`
	export DEVICE="${args[2]},${args[4]}"

	echo "////////////////// RUNNING ON $devicename ($DEVICE)"

	REP=10000 benchmark small
	REP=1 benchmark large
done < <(./lsdev | egrep "platform [0-9]+ device")
